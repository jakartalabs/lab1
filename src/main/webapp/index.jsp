<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>Lab Jakarta</title>
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin="crossorigin"/>
    <link rel="preload" as="style" href="https://fonts.googleapis.com/css2?family=Poppins:wght@600&amp;family=Roboto:wght@300;400;500;700&amp;display=swap"/>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Poppins:wght@600&amp;family=Roboto:wght@300;400;500;700&amp;display=swap" media="print" onload="this.media='all'"/>
    <noscript>
      <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Poppins:wght@600&amp;family=Roboto:wght@300;400;500;700&amp;display=swap"/>
    </noscript>
    <link href="./static/css/font-awesome/css/all.min.css?ver=1.2.0" rel="stylesheet">
    <link href="./static/css/bootstrap.min.css?ver=1.2.0" rel="stylesheet">
    <link href="./static/css/aos.css?ver=1.2.0" rel="stylesheet">
    <link href="./static/css/main.css?ver=1.2.0" rel="stylesheet">
    <link href="./static/css/site.css" rel="stylesheet">
    <noscript>
      <style type="text/css">
        [data-aos] {
            opacity: 1 !important;
            transform: translate(0) scale(1) !important;
        }
      </style>
    </noscript>
</head>
<body>
    <header>
        <div class="logo">
            <div class="hover-effect"><a href="views/tarasenko.html"><img src="./static/images/tarasenko_avatar.jpg" width="200" height="230" alt="Tarasenko"/></a></div>
            <div class="hover-effect"><a href="views/bezliudnyi.html"><img src="./static/images/bezliudnyi_avatar.jpg" width="200" height="230" alt="Bezliudnyi"/></a></div>
            <div class="hover-effect"><a href="views/bezpalko.html"><img src="./static/images/bezpalko_avatar.jpg" width="200" height="230" alt="Bezpalko"/></a></div>
        </div>
        <div class="positions">
            <h2 class="mb3">Project Manager</h2>
            <h2 class="mb3">Team Lead</h2>
            <h2 class="mb3">Software Engineer</h2>
        </div>
        <br/>
        <div class="logo">
            <div class="hover-effect"><a href="views/ivliev.html"><img src="./static/images/ivliev_avatar.jpg" width="200" height="230" alt="Bezpalko"/></a></div>
            <div class="hover-effect"><a href="views/yaremenko.html"><img src="./static/images/yaremenko_avatar.jpg" width="200" height="230" alt="Yaremenko"/></a></div>
        </div>
        <div class="positions">
            <h2 class="mb3">Quality Assurance</h2>
            <h2 class="mb3">Business Analyst</h2>
        </div>
    </header>
    <p class="title">Fantastic Project Roles</p>
    <a href="hello-servlet">Hello Servlet</a>
</body>
</html>